'use strict';
// IMC study
class Imc {
	// Constructor -> initializes some data
  constructor(name, weight, heigth) {
    this.name = name;		// 3 attributes in In mode
    this.weight = weight;
	this.heigth = heigth;
	this.imc = this.compute();	// attribute in OUT mode (to be computed!)
  }
	// Model -> do the Job!
  compute() {
    return this.weight/Math.pow(this.heigth, 2);
  }
    // View (only renders information)
  display() {
	console.log (this.name + " (" +
		this.weight + " kg, " + this.heigth + 
		" m) has the following IMC: " + this.imc);
  }
}

	// progr principal -> injection data
const list = [];
list.push(new Imc("Pilier Samoan", 135, 1.89));
list.push(new Imc("Escaladeuse", 45, 1.70));

for (let e of list)
	e.display();
/* other way to list an array:
list.forEach(p => {
  p.display();
});*/

