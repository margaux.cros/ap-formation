/*
#### TODO
Coder une classe Singleton dont la méthode getInstance retourne
toujours la même valeur (tirée au sort avec Math.random())

Ensure a class has only one instance, and provide a global point of
access to it.
#### NOTA EN JAVA:
public final class MonSingleton {
  private static MonSingleton instance = new MonSingleton();
  public static MonSingleton getlnstance() {
    return instance;
  }
  private MonSingleton() { // Math.random()...
  }
}

MonSingleton.getInstance();
*/
'use strict';

class Singleton {
  static getInstance() {
	if (this.instance === undefined) {	// calcul une fois pour toutes!!
      this.instance = Math.random();
    }
    return this.instance;
  }
}

class NotASingleton {
  getInstance() {
    return Math.random();
  }
}
function main() {
	console.log(Singleton.getInstance());	// methode de classe
	console.log(Singleton.getInstance());	// même valeur ramenée
	console.log("-------");
	console.log(new NotASingleton().getInstance());	// des choses  différentes!!
	console.log(new NotASingleton().getInstance());
}
