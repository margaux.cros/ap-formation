class FormeFactory {
    constructor(forme){
        switch(forme.type) {
            case 'Cercle' :
            return (new Cercle);

            case 'Rectangle' :
            return (new Rectangle);

            case 'Triangle' :
            return (new Triangle);
        }
    }

}
