class Employe{

    constructor(nom){
        this.nom = nom;
        this.salaire = 2000;
        this.bonus = 0;
    }

    strNom(){
        return 'Je m\'appelle ' + this.nom;
    }

    strSalaire(){
        return 'Je gagne ' + this.salaire + ' € par mois';
    }

    revenuAnnuel(){
        return this.salaire*12 + this.bonus;
    }

    strSalaireAnnuel(){
        return 'Je touche ' + this.revenuAnnuel() + ' € par an';
    }

}

