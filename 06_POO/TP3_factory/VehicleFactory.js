class VehicleFactory {

    constructor(type){
        switch(type) {
            case 'Auto' :
            return (new Auto);

            case 'Truck' :
            return (new Truck);
        }
    }
}
