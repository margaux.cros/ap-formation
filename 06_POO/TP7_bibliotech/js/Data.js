class Data {
    constructor(type, url) {
        this.type = type;
        this.load(url);
    }

    load() {
        fetch(url)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(new Error("Invalid response"));
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
            this.data = data;

            if(this.type == 'books'){
                this.displayBooks(data)
            }else if(this.type == 'users'){
                this.displayUsers(data)
            }


        })
        .catch(error => console.error(error.message));
    }

    displayBooks(data){
        let cellBooks = document.getElementById('books-list');


        data.forEach(function(val){
            cellBooks.innerHTML+=
            '<div class="col-x-5 book">'
            +'  <div class="box-image"><img src="https://adok.org/form/bibliotech/img/book_'+val.id+'.jpg"></div>'
            +'  <div>'+val.titre+'</div>'
            +'</div>';

        })

    }

    displayUsers(data){
        let cellUsers = document.getElementById('users-list');

        var usertable ='';

        data.forEach(function(val){
            usertable+=
            '<tr>'
            +'  <td>'+val.nom+'</td>'
            +'  <td>'+val.prenom+'</td>'
            +'</tr>';

        })


        cellUsers.innerHTML = usertable;
    }

    tri(index){
        var dataCopie = JSON.parse(JSON.stringify(this.data));

        if (index  ==  'nom'){
            dataCopie.sort((a, b) => (a.nom >= b.nom) ? 1 : -1);

        }else if( index == 'prenom'){
            dataCopie.sort((a, b) => (a.prenom > b.prenom) ? 1 : -1);
        }


        if (JSON.stringify(dataCopie) === JSON.stringify(this.data)){
            dataCopie = Array.prototype.reverse.call(dataCopie);
        }

        this.displayUsers(dataCopie);
        this.data = dataCopie;
        return;


    }



}
