class Test {
	constructor() {
		this.load();
	}

	load() {
		let url = "https://adok.org/form/bibliotech/users.php";
		fetch(url)
			.then(response => {
				if (!response.ok) {
					return Promise.reject(new Error("Invalid response"));
				}
				return response.json();
			})
			.then(users => {
				console.log(users);
			})
			.catch(error => console.error(error.message));
	}


}

new Test();
