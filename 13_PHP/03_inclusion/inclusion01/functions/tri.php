<?php

function triAlphabetique($a, $b)
{
    return(tri($a, $b, 'nom'));
}

function triPrixCroissant($a, $b){
    return(tri($a, $b, 'prix'));
}

function tri($a, $b, $index){
    if ($a[$index] == $b[$index]) {
        return 0;
    }
    return ($a[$index] < $b[$index]) ? -1 : 1;
}
