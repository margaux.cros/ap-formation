<?php


class User
{
    private $id;
    private $name;
    private $username;
    private $email;
    private $adressstreet;
    private $adresssuite;
    private $adresscity;
    private $adresszipcode;
    private $adressgeolat;
    private $adressgeolng;
    private $phone;
    private $website;
    private $companyname;
    private $companycatchPhrase;
    private $companybs;

    /**
     * @return mixed
     */
    public function Id()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function Name()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function Username()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function Email()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function Adressstreet()
    {
        return $this->adressstreet;
    }

    /**
     * @return mixed
     */
    public function Adresssuite()
    {
        return $this->adresssuite;
    }

    /**
     * @return mixed
     */
    public function Adresscity()
    {
        return $this->adresscity;
    }

    /**
     * @return mixed
     */
    public function Adresszipcode()
    {
        return $this->adresszipcode;
    }

    /**
     * @return mixed
     */
    public function Adressgeolat()
    {
        return $this->adressgeolat;
    }

    /**
     * @return mixed
     */
    public function Adressgeolng()
    {
        return $this->adressgeolng;
    }

    /**
     * @return mixed
     */
    public function Phone()
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function Website()
    {
        return $this->website;
    }

    /**
     * @return mixed
     */
    public function Companyname()
    {
        return $this->companyname;
    }

    /**
     * @return mixed
     */
    public function CompanycatchPhrase()
    {
        return $this->companycatchPhrase;
    }

    /**
     * @return mixed
     */
    public function Companybs()
    {
        return $this->companybs;
    }


}