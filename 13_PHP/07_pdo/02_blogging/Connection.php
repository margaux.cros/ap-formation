<?php

class Connection
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @param $host
     * @param $user
     * @param $pass
     * @param $dbname
     */
    public function __construct($host, $dbname, $user, $pass )
    {
        $this->pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
    }


    /**
     * @param string $table
     * @param string $className
     * @param string $field
     * @param mixed $value
     * @param string $operator
     * @return array
     */
    function findAllBy(string $table, string $className, string $field, mixed $value, string $operator ='=')
    {
        $query = $this->pdo->query("SELECT * FROM $table WHERE $field $operator $value");
        return $query->fetchAll(PDO::FETCH_CLASS, $className);
    }

    /**
     * @param string $table
     * @param string $className
     * @param int $value
     * @param string $operator
     * @return mixed
     */
    function findById(string $table, string $className, int $value, string $operator ='='){
        $query = $this->pdo->query("SELECT * FROM $table WHERE id $operator $value");
        return $query->fetch(PDO::FETCH_CLASS, $className);
    }
}