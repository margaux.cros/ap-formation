<?php

require 'login.php';
require 'Connection.php';
require 'User.php';

$connection = new Connection($host, $dbname, $user, $pass);
$users = $connection->findAllBy('users', 1, 1);

?>


<html lang="fr">
<head>
    <meta charset="utf-8">
    <title> liste des utilisateurs </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>

<div class="container">

    <h1>Liste des utilisateurs présents dans la base de données</h1>

    <div class="row">
        <?php
        foreach ($users as $user) {
            if ($user->getGender() == 'Male') {
                $textColor = 'text-male';
            } else {
                $textColor = 'text-female';
            }
            ?>

            <div class="card" style="width: 18rem; margin:5px;">
                <img class="card-img-top" src="<?= $user->getPhoto() ?>" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title <?= $textColor ?>"><?= $user->getFirstName() ?> <?= $user->getLastName() ?></h5>
                    <p class="card-text">
                        <i><?= $user->getSlogan() ?></i><br/>
                        email : <?= $user->getEmail() ?>
                        telephone : <?= $user->getPhone() ?>
                    </p>
                    <a href="profile?id=<?= $user->getId() ?>" class="btn btn-primary">Voir la fiche</a>
                </div>
            </div>
        <?php } ?>
    </div>

</div>

</body>
</html>
