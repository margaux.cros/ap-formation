<?php

class Connection
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * @param $host
     * @param $user
     * @param $pass
     * @param $dbname
     */
    public function __construct($host, $dbname, $user, $pass )
    {
        $this->pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
    }


    /**
     * @param $table
     * @param $field
     * @param $value
     * @param string $operator
     * @return array
     */
    function findAllBy($table, $field, $value, $operator ='=')
    {
        $query = $this->pdo->query("SELECT * FROM $table WHERE $field $operator $value");
        return $query->fetchAll(PDO::FETCH_CLASS, "User");
    }
}