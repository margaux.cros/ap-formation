<?php

require 'login.php';
require 'Connection.php';
require 'User.php';



if (!isset($_GET['id'])) {
    die(header("Location: index.php"));

} else {
    $id = intval($_GET['id']);
    $connection = new Connection($host, $dbname, $user, $pass);
    $user = $connection->findAllBy('users', 'id', $id)[0];
    ?>


    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <title> liste des utilisateurs </title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <link href="css/styles.css" rel="stylesheet">

    </head>
    <body>

    <div class="container">

        <?php


        ($user->getGender() == 'Male'? $textColor='text-male' : $textColor = 'text-female');
        ?>

        <div class="row">
            <div class="col-sm-5">
                <div class="w-100"><img class="img-fluid" src = "<?=$user->getPhoto() ?>"<br /></div>
                <i><?= $user->getSlogan() ?></i>
            </div>
            <div class="col-sm-7">
                <h1 class=" <?= $textColor ?>"><?= $user->getFirstName() ?> <?= $user->getLastName() ?></h1>
                email : <?= $user->getEmail() ?><br/>
                telephone : <?= $user->getPhone() ?> <br/>

                <a href="index.php"><button class="btn mt-3 btn-primary">Retour</button></a>
            </div>

        </div>

    </div>

    </body>
    </html>

<?php } ?>