<?php

/**
 * Class Biblio
 */
class Biblio
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var array
     */
    private array $books;

    /**
     * Biblio constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     *
     */
    public function list()
    {
        echo $this->name . '<br />';

        foreach ($this->books as $book) {
            echo $book->getTitle() . ' (' . $book->getPageNumber() . ') <br/>';
        }

    }

    /**
     * @param Book $book
     */
    public function addBook(Book $book)
    {
        $this->books[] = $book;
    }
}