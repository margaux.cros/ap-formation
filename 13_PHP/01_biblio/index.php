<?php

include 'Biblio.php';
include 'Book.php';

$biblio = new Biblio("Bibliothèque du Web");
$biblio->addBook(new Book("La bible JavaScript", 640));
$biblio->addBook(new Book("SQL par l'exemple", 423));
$biblio->addBook(new Book("Tout PHP", 1267));
$biblio->list();
