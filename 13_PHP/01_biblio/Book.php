<?php

/**
 * Class Book
 */
class Book
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var int
     */
    private int $pageNumber;

    /**
     * Book constructor.
     * @param string $title
     * @param int $pageNumber
     */
    public function __construct(string $title, int $pageNumber)
    {
        $this->title = $title;
        $this->pageNumber = $pageNumber;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}