<?php
function verifInput($POST)
{
    $errors = array();
    foreach ($POST as $key => $value) {

        if (gettype($value) == "string") {
            if (!$value OR strlen($value) == 0) {
                $errors[$key] = $key;
            }
        } elseif (gettype($value) == "array" and sizeof($value) > 0) {
            $send = false;
            foreach ($value as $val) {
                if (!$val == '') {
                    $send = true;
                }
            }
            if (!$send) {
                $errors[$key] = $key;
            }
        }
    }

    return $errors;
}

function addErrorClass($errors, $value)
{
    if (isset($errors[$value])) {
        return 'text-danger';
    }

    return '';
}

function addValue($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return '';
}

function getImageLink($repoDestiniation, $key)
{

    if (isset($_FILES[$key]) AND ($_FILES[$key]['name'] != '')) {
        $image = $_FILES[$key];
        $extension = explode('.', $image['name'])[1];
        $uploadfile = $repoDestiniation . '/' . $key . '_' . time() . '.' . $extension;
        if (move_uploaded_file($image['tmp_name'], $uploadfile)) {
            return $uploadfile;
        }
    }

    return false;
}

$POST = array();
if (isset($_POST['submit'])) {
    $POST = array(
        'titre' => $_POST['titre'],
        'annonce' => $_POST['annonce'],
        'prix' => $_POST['prix'],
        'photos' => array('photo1' => $_FILES['photo1']['name'], 'photo2' => $_FILES['photo2']['name'], 'photo3' => $_FILES['photo3']['name'])
    );
}
$errors = verifInput($POST);;


if (!is_dir(dirname(__FILE__) . "\images")) {
    mkdir(dirname(__FILE__) . "\images");
}
$repoDestiniation = "images";

?>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title> lebonplan </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>

<?php include 'header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <form method="post" action="index.php" class="form" enctype="multipart/form-data">
                <div class="form-group mb-2">
                    <h3 class="<?= addErrorClass($errors, 'titre') ?>">Titre</h3>
                    <input value="<?= addValue('titre') ?>" name="titre" type="text" class="form-control "
                           placeholder="Titre">
                </div>
                <div class="form-group mb-2">
                    <h3 class="<?= addErrorClass($errors, 'annonce') ?>">Annonce</h3>
                    <textarea name="annonce" class="form-control" rows="4"
                              placeholder="Annonce"><?= addValue('annonce') ?></textarea>
                </div>
                <div style="display: flex; flex-wrap: wrap" class="form-group mb-2">
                    <h3 style="min-width: 100%" class=" <?= addErrorClass($errors, 'prix') ?>">Prix</h3>
                    <input name="prix" value="<?= addValue('prix') ?>" style="width:55%"
                           class="form-control form-control-sm mr-3 d-inline" type="number"
                           placeholder="10">
                    <div class="text-right" style="width:2%">€</div>
                </div>
                <div class="form-group mb-2">
                    <div class="form-group">
                        <h3 class="<?= addErrorClass($errors, 'photos') ?>">Photos</h3>
                        <input type="hidden" name="MAX_FILE_SIZE" value="30000"/>
                        <input name="photo1" type="file" class="form-control-file mb-1">
                        <input name="photo2" type="file" class="form-control-file mb-1">
                        <input name="photo3" type="file" class="form-control-file mb-1">
                    </div>
                </div>
                <button type="submit" name="submit" class="btn btn-orange mb-2">Soumettre</button>
            </form>

            <?php if (sizeof($errors) > 0) {
                ?>
                <div class="alert alert-danger" role="alert">
                    <strong>Erreur: </strong> Veuillez renseigner les champs
                    <ul>
                        <?php foreach ($errors as $error) { ?>
                            <li><?= $error ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-5">
            <?php if (sizeof($errors) == 0 AND sizeof($POST) > 0) {
                ?>
                <h3>Prévisualiation</h3>
                <h2><?= $POST['titre'] ?></h2>
                <p><?= nl2br($POST['annonce']) ?></p>

                <div class="bg-orange text-light p-2 mb-3 text-right"><?= $POST['prix'] ?> €</div>

                <?php foreach ($_FILES as $key => $FILE) {
                    if ($FILE['name'] != '') {
                        ?>
                        <img src="<?= getImageLink('images', $key) ?>" class="img-fluid">
                    <?php }
                } ?>

            <?php } ?>

        </div>
    </div>
</div>

</body>
</html>
