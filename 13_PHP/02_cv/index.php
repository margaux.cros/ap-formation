<?php

include 'database-cv.php';
setlocale(LC_TIME, "fr_FR");
function age($date)
{
    $age = date('Y') - date("Y", strtotime($date));;
    if (date('md') < date('md', strtotime($date))) {
        return $age - 1;
    }
    return $age;
}
$diplomes = array_reverse($cv['diplomes'], true);
$experiences = array_reverse($cv['experiences'], true);

?>





<h1><?= $cv['nom'] ?> - <?= $cv['metier'] ?></h1>
<img src="<?= $cv['photo'] ?>"><br/>
<?= age($cv['date_naissance']) ?> ans <br/>
<?= $cv['adresse'] ?><br/>
Téléphone : <?= substr(chunk_split($cv['tel'], 2, '-'), 0, -1) ?><br/>

<h2>Diplômes</h2>
<?php foreach ($diplomes as $nom => $diplome) { ?>
    <?= $diplome ?> : <?= $nom ?><br/>
<?php } ?>

<h2>Expériences</h2>
<?php foreach ($experiences as $experience) {
    $debut = strftime("%d %B %Y", strtotime($experience['debut']));
    if ($experience['fin'] == 'now') {
        $fin = '<b> à maintenant</b>';
    } else {
        $fin = 'au ' . strftime("%d %B %Y", strtotime($experience['fin']));
    }
    ?>
    <?= $debut ?>  <?= $fin ?> : <?= $experience['libelle'] ?><br/>
<?php } ?>

<h2>Compétences</h2>
<table>
    <?php foreach ($cv['competences'] as $nom => $competence) { ?>
        <tr>
            <td><?= strtoupper($nom) ?></td>
            <?php for ($i = 1; $i < 6; $i++) {
                if ($competence >= $i) { ?>
                    <td><img style="width:40px" src="star.png"></td>
                    <?php
                } else { ?>
                    <td style="filter:grayscale(1)"><img style="width:40px" src="star.png"></td>
                <?php } ?>
            <?php } ?>
        </tr>
    <?php } ?>
</table>



