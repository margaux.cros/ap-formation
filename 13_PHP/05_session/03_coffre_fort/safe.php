<?php
session_start();
include 'data/users.php';
setlocale(LC_TIME, "fr_FR");
?>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title> Coffre fort </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>

<?php include 'header.php'; ?>
<div class="container">
    <?php
    if (isset($_SESSION['nameLogged'])) {
        $name = $_SESSION['nameLogged'];
        $user = $users[$name];
        ?>

        <h2><?=ucfirst($name) ?>, it's wonderful to see you again!</h2>
        <div class="alert alert-info" role="alert">
            Connecté le  <?= strftime('%A %e %B à  %H:%M:%S',  $_SESSION['time'])?> sur <?=$_SESSION['ip']?>
        </div>
        <table class="table table-hover">
            <thead class="thead">
            <tr>
                <th scope="col">Type</th>
                <th scope="col">Amount</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Gold</th>
                <td><?=$user['or'] ?></td>
            </tr>
            <tr>
                <th scope="row">Iron</th>
                <td><?=$user['argent'] ?></td>
            </tr>
            <tr>
                <th scope="row">Bronze</th>
                <td><?=$user['bronze'] ?></td>
            </tr>
            </tbody>
        </table>

        <form method="post" action="login.php">
            <button type="submit" name="logout" class="btn btn-danger mb-2">Logout</button>
        </form>



    <?php } else { ?>
        <div class="alert alert-warning" role="alert">
            <strong>Warning!</strong> You need to be identify to access the safe.
        </div>

        <a href="index.php">
            <buttontype="button" class="btn btn-warning">Back to home</button>
        </a>
    <?php } ?>


</div>

</body>

