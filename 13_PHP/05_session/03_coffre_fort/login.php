<?php
session_start();

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;

}

include 'data/users.php';
$_SESSION['error'] = true;

if (isset($_POST['submit'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    foreach ($users as $name => $user) {
        if ($username == $name and $password == $user['password']) {
            unset($_SESSION['error']);
            $_SESSION['nameLogged'] = $name;
            $_SESSION['time'] = time();
            $_SESSION['ip'] = getIp();
            die(header("Location: safe.php"));
        }
    }
} else {
    unset($_SESSION['nameLogged']);
    unset($_SESSION['error']);
}
die(header("Location: index.php"));

