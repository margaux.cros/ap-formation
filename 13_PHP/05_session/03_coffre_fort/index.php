<?php
session_start();
?>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title> Coffre fort </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>

<?php include 'header.php'; ?>
<div class="container">
    <form method="post" action="login.php" class="form-inline">
        <div class="form-group mb-2">
            <input name="username" type="text" class="form-control" placeholder="Username">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <input name="password" type="password" class="form-control" placeholder="Password">
        </div>
        <button type="submit" name="submit" class="btn btn-primary mb-2">Confirm identity</button>
    </form>

    <?php
    if (isset($_SESSION['error'])) {
        ?>
        <div class="alert alert-danger" role="alert">
            <strong>Ooops! </strong> Your username and/or password do not match.
        </div>
    <?php } ?>
</div>

</body>
</html>