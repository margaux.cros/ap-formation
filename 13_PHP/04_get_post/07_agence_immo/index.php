<?php
require 'data/annonces.php';
require 'functions/functions.php';



?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="desciption" content="commentaire décrivant la page actuelle">
    <meta name="author" content="Margaux">
    <title> Agence Immo + </title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/styles.css" rel="stylesheet">

</head>
<body>
<?php require 'views/header.php'; ?>

<div class="container-fluid">
    <div class="row">
        <section id="search" class="col-md-3">
            <?php require 'views/search.php'; ?>
        </section>
        <section id="search" class="col-md-6">
            <?php require 'views/body.php'; ?>
        </section>
        <section id="search" class="col-md-3">
            <?php require 'views/filters.php'; ?>
        </section>
    </div>
</div>


</body>
</html>
