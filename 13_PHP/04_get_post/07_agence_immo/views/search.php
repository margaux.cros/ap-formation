<div class="container-fluid">
    <h2 class="bottom-dark">Recherche</h2>
    <form action="index.php" method="post">
        <div class="form-group">
            <select name="price" class="form-control">
                <option value="all"
                    <?= isSelected('price') ?>>
                    tous les prix
                </option>
                <option value="price_1"
                    <?= isSelected('price', 'price_1') ?>>
                    - de 100 000€
                </option>
                <option value="price_2"
                    <?= isSelected('price', 'price_2') ?>>
                    entre 100 000€ et 125 000€
                </option>
                <option value="price_3"
                    <?= isSelected('price', 'price_3') ?>>
                    entre 125 000€ et 150 000€
                </option>
                <option value="price_4"
                    <?= isSelected('price', 'price_4') ?>>
                    entre 150 000€ et 175 000€
                </option>
                <option value="price_5"
                    <?= isSelected('price', 'price_5') ?>>
                    entre 175 000€ et 200 000€
                </option>
                <option value="price_6"
                    <?= isSelected('price', 'price_6') ?>>
                    + de 200 000€
                </option>
            </select>
        </div>

        <div class="form-group">
            <select name="surface" class="form-control">
                <option
                    <?= isSelected('surface') ?>>
                    toutes les surfaces
                </option>
                <option value="surface_1"
                    <?= isSelected('surface', 'surface_1') ?>>
                    entre 50m2 et 75m2
                </option>
                <option value="surface_2"
                    <?= isSelected('surface', 'surface_2') ?>>
                    entre 75m2 et 100m2
                </option>
                <option value="surface_3"
                    <?= isSelected('surface', 'surface_3') ?>>
                    entre 100m2 et 1252
                </option>
                <option value="surface_4"
                    <?= isSelected('surface', 'surface_4') ?>>
                    entre 125m2 et 150m2
                </option>
                <option value="surface_5"
                    <?= isSelected('surface', 'surface_5') ?>>
                    + de 150m2
                </option>
            </select>
        </div>

        <div class="form-group">
            <div class="form-check">
                <input <?= isChecked('jardin') ?> class="form-check-input" type="checkbox" id="jardin" name="jardin">
                <label class="form-check-label" for="jardin">
                    Jardin
                </label>
            </div>
            <div class="form-check">
                <input <?= isChecked('terrasse') ?> class="form-check-input" type="checkbox" id="terrasse"
                                                    name="terrasse">
                <label class="form-check-label" for="terrasse">
                    Terrasse
                </label>
            </div>
            <div class="form-check">
                <input <?= isChecked('garage') ?> class="form-check-input" type="checkbox" id="garage" name="garage">
                <label class="form-check-label" for="garage">
                    Garage
                </label>
            </div>
        </div>

        <input class="btn btn-primary" input type="submit" value="Valider" name="send">
    </form>
</div>
