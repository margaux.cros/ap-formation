<?php

$items = $annonces;

if (isset($_POST['send'])) {

    switch ($_POST['price']) {
        case 'price_1':
            $PriceMin = 0;
            $PriceMax = 100000;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
        case 'price_2':
            $PriceMin = 100000;
            $PriceMax = 125000;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
        case 'price_3':
            $PriceMin = 125000;
            $PriceMax = 150000;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
        case 'price_4':
            $PriceMin = 150000;
            $PriceMax = 175000;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
        case 'price_5':
            $PriceMin = 175000;
            $PriceMax = 200000;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
        case 'price_6':
            $PriceMin = 200000;
            $PriceMax = 9999999999;
            $items = filtMinMax($items, 'prix', $PriceMin, $PriceMax);
            break;
    }

    switch ($_POST['surface']) {
        case 'surface_1':
            $SurfaceMin = 50;
            $SurfaceMax = 75;
            $items = (filtMinMax($items, 'surface', $SurfaceMin, $SurfaceMax));
            break;
        case 'surface_2':
            $SurfaceMin = 75;
            $SurfaceMax = 100;
            $items = (filtMinMax($items, 'surface', $SurfaceMin, $SurfaceMax));
            break;
        case 'surface_3':
            $SurfaceMin = 100;
            $SurfaceMax = 125;
            $items = (filtMinMax($items, 'surface', $SurfaceMin, $SurfaceMax));
            break;
        case 'surface_4':
            $SurfaceMin = 125;
            $SurfaceMax = 150;
            $items = (filtMinMax($items, 'surface', $SurfaceMin, $SurfaceMax));
            break;
        case 'surface_5':
            $SurfaceMin = 150;
            $SurfaceMax = 9999;
            $items = (filtMinMax($items, 'surface', $SurfaceMin, $SurfaceMax));
            break;
    }


    if (isset($_POST['jardin'])) {
        $items = filtList($items, 'jardin', true);
    }
    if (isset($_POST['terrasse'])) {
        $items = filtList($items, 'terrasse', true);
    }
    if (isset($_POST['garage'])) {
        $items = filtList($items, 'garage', true);
    }

}


if (isset($_GET['ville'])) {
    $citySelected = $_GET['ville'];
    $items = filtList($items, 'ville', $citySelected);
}
?>

<div class="container-fluid">
    <h2 class="bottom-dark">Nos annonces immobilières :</h2>
    <?php if (empty($items)) { ?>
        <div class="alert alert-warning p-3">Aucun élément ne correspond à votre recherche</div>
    <?php } else {
        foreach ($items as $annonce) {
            $strJardin = ($annonce['jardin'] ? 'oui' : 'non');
            $strTerrasse = ($annonce['terrasse'] ? 'oui' : 'non');
            $strGarage = ($annonce['garage'] ? 'oui' : 'non');
            ?>
            <div class="product-item bottom-dark">
                <h3>Maison à <?= $annonce['ville'] ?> (réf <?= $annonce['id'] ?>)</h3>
                <div class="row">
                    <div class="col-6">
                        <img class="img-fluid" src="img/<?= $annonce['img'] ?>">
                    </div>
                    <div class="col-6">
                        <h4>Prix : <?= $annonce['prix'] ?> €</h4>
                        <h4>Surface : <?= $annonce['surface'] ?> m2</h4>
                        Options :
                        <ul>
                            <li>Jardin : <?= $strJardin ?></li>
                            <li>Terrasse : <?= $strTerrasse ?></li>
                            <li>Garage : <?= $strGarage ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php }
    } ?>
</div>