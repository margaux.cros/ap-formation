<?php $cities = array(); ?>
<div class="container-fluid">
    <h2 class="bottom-dark">Filtrer par ville</h2>
    <ul>
        <?php foreach ($annonces as $annonce) {
            if (!in_array($annonce['ville'], $cities)) {
                $cities[] = $annonce['ville'];
                ?>
                <li><a href=?ville=<?= $annonce['ville'] ?>><?= $annonce['ville'] ?></a></li>
            <?php } ?>
        <?php } ?>
    </ul>
    <a href="index.php"><button type="button" class="btn btn-primary">Supprimer le filtre</button></a>
</div>