<?php
$annonces = array(
  array(
    'id' => 512,
    'ville' => 'Toulouse',
    'prix' => 129000,
    'img' => '512.jpg',
    'surface' => 85,
    'jardin' => false,
    'garage' => true,
    'terrasse' => true
  ),
  array(
    'id' => 510,
    'ville' => 'Strasbourg',
    'prix' => 165000,
    'img' => '510.jpg',
    'surface' => 98,
    'jardin' => true,
    'garage' => true,
    'terrasse' => false
  ),
  array(
    'id' => 324,
    'ville' => 'Paris',
    'prix' => 189000,
    'img' => '324.jpg',
    'surface' => 65,
    'jardin' => false,
    'garage' => false,
    'terrasse' => true
  ),
  array(
    'id' => 34,
    'ville' => 'Bordeaux',
    'prix' => 145000,
    'img' => '34.jpeg',
    'surface' => 60,
    'jardin' => true,
    'garage' => true,
    'terrasse' => false
  ),
  array(
    'id' => 647,
    'ville' => 'Marseille',
    'prix' => 113000,
    'img' => '647.jpg',
    'surface' => 55,
    'jardin' => false,
    'garage' => false,
    'terrasse' => false
  ),
  array(
    'id' => 456,
    'ville' => 'Marseille',
    'prix' => 230000,
    'img' => '456.jpeg',
    'surface' => 95,
    'jardin' => true,
    'garage' => true,
    'terrasse' => true
  ),
  array(
    'id' => 25,
    'ville' => 'Strasbourg',
    'prix' => 99000,
    'img' => '25.jpg',
    'surface' => 55,
    'jardin' => true,
    'garage' => false,
    'terrasse' => false
  ),
  array(
    'id' => 552,
    'ville' => 'Paris',
    'prix' => 290000,
    'img' => '552.jpeg',
    'surface' => 125,
    'jardin' => false,
    'garage' => true,
    'terrasse' => true
  ),
  array(
    'id' => 189,
    'ville' => 'Toulouse',
    'prix' => 140000,
    'img' => '189.jpg',
    'surface' => 75,
    'jardin' => true,
    'garage' => true,
    'terrasse' => true
  ),
  array(
    'id' => 198,
    'ville' => 'Toulouse',
    'prix' => 168000,
    'img' => '198.jpg',
    'surface' => 225,
    'jardin' => true,
    'garage' => true,
    'terrasse' => true
  ),
  array(
    'id' => 101,
    'ville' => 'Paris',
    'prix' => 195000,
    'img' => '101.jpg',
    'surface' => 68,
    'jardin' => false,
    'garage' => true,
    'terrasse' => true
  ),

);

?>
