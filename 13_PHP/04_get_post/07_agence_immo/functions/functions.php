<?php

function filtList($elements, $index, $filt)
{
    $data = array();
    foreach ($elements as $element) {
        if ($element[$index] == $filt) {
            $data[] = $element;
        }
    }
    return $data;
}

function filtMinMax($elements, $index, $min, $max)
{
    $data = array();
    foreach ($elements as $element) {
        if ($element[$index] > $min and $element[$index] < $max) {
            $data[] = $element;
        }
    }

    return $data;
}

function isChecked($index)
{
    if (isset($_POST[$index])) {
        return 'checked';
    }

    return '';
}

function isSelected($index, $value = 'all')
{
    if ((!isset($_POST[$index]) AND $value == 'all') OR (isset($_POST[$index]) AND $_POST[$index] == $value)) {
        return 'selected';
    }

    return '';
}
