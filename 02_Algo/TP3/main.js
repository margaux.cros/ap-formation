function main() {
/*
    var tabl = [5, 6, 1, 3];

    trier(tabl);
    console.log(tabl);*/

    var paul = [12, 17, 2, 5],
        jean = [6, 8, 14],
        elsa = [15, 19, 12];

    var classe = ['Paul', 'Jean', 'Elsa'];
    var notes = [paul, jean, elsa];
    
    console.log(bestNote(classe, notes));

} //end main

function trier(tabl) {
    //tabl.sort();
    var longueur = tabl.length;

    for (var i = 0; i < longueur; i++) {
        for (var j = i + 1; j < longueur; j++) {
            if (compare(tabl[i], tabl[j])) {
                var temp = tabl[j];
                tabl[j] = tabl[i];
                tabl[i] = temp;
            }
        }
    }

    return tabl
}

function compare(a, b) {
    if (a >= b) {
        return true
    } else {
        return false
    }
}

function bestNote(classe, notes) {
    var bestNote = 0;
    var bestEleve = '';
    for (var i = 0; i < notes.length; i++) {

        var note = Math.max(...notes[i]);
        if (note > bestNote) {
            bestNote = note;
            var bestEleve=classe[i];
        }
    }

    return bestEleve +' a obtenu la meilleure note au controle de sciences : '+ bestNote;
}

