## Enoncé

Gérer un carnet d'adresses dans le navigateur (data stockées dans le 'localStorage')
avec JQuery/css (cf maquette)
## Consignes
- utiliser JQuery
- déclarer dans le html toute la partie statique


### CSS
- utiliser les fontes Google (font-awesome.min)

## Actions demandées
- consulter: window.localStorage (pour le stockage/la lecture)
- sérialiser la data dans ce localStorage
- consacrer un fichier js au métier : la gestion du carnet d'adresses
- isoler les divers évenements dans un fichier js


