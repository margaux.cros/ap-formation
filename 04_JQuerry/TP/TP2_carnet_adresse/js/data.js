//========================== ENREGISTREMENT LOCAL STORAGE ============================= 
function saveStorage(key, value){
    localStorage.setItem(key, JSON.stringify(value));
    return true;
}

function getStorage(key){
    var storedNames = JSON.parse(localStorage.getItem(key));
    return storedNames;
}

function setData(key, n){
    data=getStorage(key);
    data.push(n);
    return data;
}

function updateData(key, index, n){
    data=getStorage(key);
    data[index]=n;
    return data;
    
}
