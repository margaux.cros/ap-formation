//========================== MANAGER =============================
jQuery(document).ready(function ($) {

    var modi = -1;

    //----------------functions-------------

    $('#submit-button').click(function () {
        var civilite = $('#civilite').val();
        var nom = $('#nom').val();
        var prenom = $('#prenom').val();
        var tel = $('#tel').val();

        //alert(civilite + ' ' + nom + ' ' + prenom + ' ' + tel);

        if (modi >= 0) {
             var modiContact = new Personne(civilite, nom, prenom, tel);
             var modiCarnetAdress = updateData('carnetAdress', modi, modiContact)
             saveStorage('carnetAdress', modiCarnetAdress);
        } else {
            var contact = new Personne(civilite, nom, prenom, tel);
            var setCarnetAdress = setData('carnetAdress', contact);
            saveStorage('carnetAdress', setCarnetAdress);
        }

    });

    $('#butt-ajouter-contact').click(function () {
        $('#section-ajouter-contact').css('display', 'block');
    });
    
    
    $('#butt-supp-contact').click(function () {
        if (modi>=0){
            
        }
    });



    //-----------------main--------------

    var CarnetAdress_A = getStorage('carnetAdress');
    //console.log(CarnetAdress_A);
    var i = 0;
    CarnetAdress_A.forEach(function (personne) {
        $('#liste-contact').append('<li i="' + i + '">' + personne.prenom + ' ' + personne.nom + '</li>');
        i++;
    });

    $('#liste-contact li').on('click', function () {
        $('#section-ajouter-contact').css('display', 'block');
        var idCcont = $(this).attr('i');
        modi = idCcont;
        affiCivilite = CarnetAdress_A[idCcont].civilite;
        affiNom = CarnetAdress_A[idCcont].nom;
        affiPrenom = CarnetAdress_A[idCcont].prenom;
        affiTel = CarnetAdress_A[idCcont].telephone;

        $('#civilite').attr("value", affiCivilite);
        $('#nom').attr("value", affiNom);
        $('#prenom').attr("value", affiPrenom);
        $('#tel').attr("value", affiTel);
    });

});
