jQuery(document).ready(function ($) {

    var $checkboxes = $('input[type="checkbox"]');

    $checkboxes.change(function () {
        var count = $checkboxes.filter(':checked').length;
        $('#counter').text('nombre de cases cochées : ' + count);
    });

    $('#red-color-text p').click(function () {
        $('#red-color-text p').css('color', 'red');
    });

    $('#carre').click(function () {
        alert();
        $('#carre').animate({
            right:'100px',
            width:'100px',
        },"slow");
        $('#carre').animate({
            height:'100px',
        },"slow");
    });


});
