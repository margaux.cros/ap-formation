<?php

class Connection
{
    protected $pdo;

    /**
     * @param $host
     * @param $user
     * @param $password
     * @param $port
     */
    public function __construct($host, $user, $password, $dbname, $port)
    {
        $this->pdo = new \PDO("mysql:host=$host;dbname=$dbname;port=$port", $user, $password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(\PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES utf8');
    }


    public function request($request, $fetch = true)
    {
        $query = $this->pdo->prepare($request);
        $query->execute();

        if($fetch){
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }else{
            return $query->fetchAll();
        }
    }
}

