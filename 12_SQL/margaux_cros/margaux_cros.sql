-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 24 mars 2020 à 14:46
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `margaux_cros`
--

-- --------------------------------------------------------

--
-- Structure de la table `content`
--

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `id_c` int(11) NOT NULL AUTO_INCREMENT,
  `cName` varchar(50) NOT NULL,
  `coeff` smallint(6) NOT NULL,
  PRIMARY KEY (`id_c`),
  UNIQUE KEY `cName` (`cName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `content`
--

INSERT INTO `content` (`id_c`, `cName`, `coeff`) VALUES
(1, 'Mathématiques', 6),
(2, 'Sciences de l\'ingénieur', 8),
(3, 'Français', 3),
(4, 'Histoire géographie', 2);

-- --------------------------------------------------------

--
-- Structure de la table `eval`
--

DROP TABLE IF EXISTS `eval`;
CREATE TABLE IF NOT EXISTS `eval` (
  `id_e` int(11) NOT NULL AUTO_INCREMENT,
  `dateEval` date DEFAULT NULL,
  `note` smallint(6) DEFAULT NULL,
  `id_s` int(11) NOT NULL,
  `id_c` int(11) NOT NULL,
  PRIMARY KEY (`id_e`),
  KEY `id_c` (`id_c`),
  KEY `id_s` (`id_s`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `eval`
--

INSERT INTO `eval` (`id_e`, `dateEval`, `note`, `id_s`, `id_c`) VALUES
(1, '2020-03-24', 15, 1, 2),
(2, '2020-03-24', 7, 2, 2),
(3, '2020-03-03', 10, 5, 3),
(4, '2020-03-03', 10, 4, 3),
(7, '2020-03-23', 15, 2, 1),
(8, '2020-03-07', 6, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id_s` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `birthDate` date DEFAULT NULL,
  `sGroup` varchar(90) DEFAULT NULL,
  `fullName` varchar(45) GENERATED ALWAYS AS (concat(`firstName`,' ',`lastName`)) VIRTUAL,
  PRIMARY KEY (`id_s`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `student`
--

INSERT INTO `student` (`id_s`, `lastName`, `firstName`, `birthDate`, `sGroup`) VALUES
(1, 'Polnareff', 'Michel', NULL, 'L'),
(2, 'Balavoine', 'Daniel', NULL, 'L'),
(3, 'Baker', 'Josephine', NULL, 'S'),
(4, 'Rossi', 'Tino', NULL, 'ES'),
(5, 'Dion', 'Céline', NULL, 'STMG');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `student_list`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `student_list`;
CREATE TABLE IF NOT EXISTS `student_list` (
`sGroup` varchar(90)
,`fullName` varchar(45)
,`average` decimal(9,4)
);

-- --------------------------------------------------------

--
-- Structure de la vue `student_list`
--
DROP TABLE IF EXISTS `student_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student_list`  AS  select `student`.`sGroup` AS `sGroup`,`student`.`fullName` AS `fullName`,avg(`eval`.`note`) AS `average` from (`student` join `eval` on((`student`.`id_s` = `eval`.`id_s`))) group by `student`.`id_s` order by `average` ;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `eval`
--
ALTER TABLE `eval`
  ADD CONSTRAINT `id_c` FOREIGN KEY (`id_c`) REFERENCES `content` (`id_c`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_s` FOREIGN KEY (`id_s`) REFERENCES `student` (`id_s`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
