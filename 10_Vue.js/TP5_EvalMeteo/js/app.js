const app = new Vue({
    el: '#app',
    data : {
        citiesToObserve : [],
        city:null,
        cityDetails : null,

        weatherIconSrc  '',

        cityTemperature : '',
        cityWindSpeed :'',
        cityWindDirection:'',
        cityDateFr : '',
        cityCoord : '',

    },
    mounted : function(){
        this.getCitiesToObserve();
    },


    methods : {
        getCitiesToObserve() {
            axios
            .get('http://formation.webboy.fr/liste_des_villes.php')
            .then(response => {
                this.citiesToObserve = response.data;
            })
            .catch(error => {
                console.log(error)
            });
        },

        getCitiesDetails() {
            axios
            .get('http://api.openweathermap.org/data/2.5/weather?id='+this.city.id+'&APPID=b7e66aebecb956d08cc56313ca276b13')
            .then(response => {
                this.cityDetails = response.data;

                this.weatherIconSrc = 'http://openweathermap.org/img/w/'+this.cityDetails.weather[0].icon+'.png'

                console.log(this.cityDetails.weather[0].icon);
                this.cityTemperature = (this.cityDetails.main.temp - 273.15).toFixed(2) +' °C';
                this.cityWindSpeed = (this.cityDetails.wind.speed * 3.6).toFixed(2) + ' km/h';
                // console.log(this.cityDetails.wind.deg);
                this.cityWindDirection = 'rotate('+(this.cityDetails.wind.deg) + 'deg)';
                this.setCityDateFr();

                this.cityCoord = this.cityDetails.coord.lon+'°N, '+this.cityDetails.coord.lat+'°O'
            })
            .catch(error => {
                console.log(error)
            });
        },

        setCityDateFr: function(){
            date=new Date(this.cityDetails.dt *1000);
            // console.log(date);
            var options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour:"numeric", minute: "numeric"};
            this.cityDateFr = date.toLocaleDateString("fr-FR", options);
        }

    }
});
