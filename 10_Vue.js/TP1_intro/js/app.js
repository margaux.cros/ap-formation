    const app = new Vue({
        el: '#app',
        data: {
            message: 'Hello !', // String
            link : 'http://moodle.webboy.fr/course/view.php?id=134', //lien
            title : 'lien vers moodle :)',

            success : true,

            persons : ['Marion', 'Claire', 'Amélie', 'Jade'],

            currentDate : null,

            nom:"dupont",
            prenom:"dAvid",

            story: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Aliquet porttitor lacus luctus accumsan tortor posuere ac. Turpis egestas sed tempus urna et pharetra pharetra. Scelerisque viverra mauris in aliquam sem. Habitant morbi tristique senectus et netus et malesuada fames ac. Varius morbi enim nunc faucibus. Commodo nulla facilisi nullam vehicula ipsum a arcu cursus. Consequat nisl vel pretium lectus. Volutpat consequat mauris nunc congue nisi vitae. Vulputate eu scelerisque felis imperdiet. Nisl nisi scelerisque eu ultrices vitae auctor.',
            storyShow : true,
            storyButton : 'show'
        },

        created: function() {
            console.log('je suis le created');
            this.getFrenchDate();
        },


        computed:{
            getFullName: function(){
                this.prenom.charAt(0).toUpperCase() + this.prenom.toLowerCase().slice(1)
                return `Votre nom complet est ${this.prenom.charAt(0).toUpperCase() + this.prenom.toLowerCase().slice(1)} ${this.nom.toUpperCase()}`
            }
        },

        filters: {
            excerpt : function(value, limit){
                return value.substr(0, limit);
            },
        },

        methods: {
            close: function() {
                this.message = 'fermé';
                this.success = false;
            },

            getFrenchDate : function() {
                date=new Date();
                var options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour:"numeric", minute: "numeric"};
                this.currentDate = date.toLocaleDateString("fr-FR", options);
            },

            showFullStory : function() {
                if (this.storyShow ) {
                    this.storyShow = false
                    this.storyButton = 'hide'
                }else{
                    this.storyShow = true
                    this.storyButton = 'show'
                }
            }
        }
    })

    //attribut speciaux :
    //afficher une variable : {{  var }}
    //modifier attricut :href="mon lien"
    //if : v-if
    //show : v-show (passe en display none)
    //else : v-else pour afficher autre chose en cas d'echec dans la condiction du if
    //for : v-for <li v-for="person in persons">{{ person }}</li>
    //event : v-on ou @
    //
