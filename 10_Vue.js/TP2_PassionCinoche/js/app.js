const app = new Vue({
    el: '#app',
    data : {
        endURL : '?api_key=bbb642504883601d7bf00b3f6f2fcb2b&language=en-US&external_source=imdb_id',
        link : 'https://api.themoviedb.org/3/discover/movie?api_key=bbb642504883601d7bf00b3f6f2fcb2b',
        searchLink : 'https://api.themoviedb.org/3/search/movie?api_key=bbb642504883601d7bf00b3f6f2fcb2b&language=en-US&page=1&include_adult=false&query=',
        findLink : 'https://api.themoviedb.org/3/movie/',
        filmList : null,
        randFilmList : [],
        selectedFilm : null,
        filmName : '',
        filmSearchList : null
    },
    mounted : function(){
        axios
        .get(this.link)
        .then(response => {
            this.filmList = response.data.results;
            this.setRandFilmList();
            this.startInterval();
        })
        .catch(error => {
            console.log(error)
        });


    },
    filters : {
        excerpt : function(value, limit){
            return value.substr(0, limit) + ' ...';
        },
    },
    methods : {
        showfilm : function(id){
            axios
            .get(this.findLink+id+this.endURL)
                .then(response => (this.selectedFilm = response.data))
                .catch(error => {
                    console.log(error)
                })

        },

        getFrenchDate : function(strDate) {
            if(typeof strDate == 'string' ){
                timestamp = Date.parse(strDate);
                date=new Date(timestamp);
            }else{
                date= new Date();
            }
            var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
            return (date.toLocaleDateString("fr-FR", options));
        },

        searchFilm : function(){
            if( this.filmName.length > 3){
                axios
                .get(this.searchLink+encodeURI(this.filmName))
                .then(response => (this.filmSearchList = response.data.results))
                .catch(error => {
                    console.log(error)
                })
            }else {
                this.filmSearchList=null;
            }
        },

        setRandFilmList() {
            var filmNumber = this.filmList.length;
            var filmList = this.filmList;
            this.randFilmList = [];
            for (var i=0; i<3; i++){
                rand = Math.floor(Math.random() * Math.floor(filmNumber))
                this.randFilmList.push(filmList[rand]);
                filList = filmList.slice(rand, filmNumber);
            }

        },

        startInterval() {
            setInterval(() => {
                this.setRandFilmList();
            }, 8000)

        }
    }
});

