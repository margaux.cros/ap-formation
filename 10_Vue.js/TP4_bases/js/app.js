const app = new Vue({
    el: '#app',
    data : {
        hour : '',
        name : '',
        email : '',
    },
    mounted : function(){
        this.getCurrentHour();
        this.startInterval();
    },
    methods : {
        getCurrentHour: function(){
            date = new Date();
            var options =  {hour : 'numeric', minute : 'numeric', second : 'numeric'};
            this.hour = (date.toLocaleTimeString("fr-FR", options));
        },
        startInterval() {
            setInterval(() => {
                this.getCurrentHour();
            }, 1000)

        }
    }
});


