


function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () { // déclarer l ‘évènement
        if (this.readyState == 4 && this.status == 200) { // succès !! → injection fichier
            document.getElementById("demo").innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", "text.txt", true);
    xhttp.send();
}

function loadJson() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () { // déclarer l ‘évènement
        if (this.readyState == 4 && this.status == 200) { // succès !! → injection fichier
            var table = JSON.parse(this.responseText);

            console.log(table);
            
            

        }
    };
    xhttp.open("GET", "data.json", true);
    xhttp.send();
    
}

function createList(tabl, id) {
    document.getElementById(id).innerHTML = '<ul>';
    tabl.forEach(function () {
        document.getElementById("liste").innerHTML = '<li>' + 'ok' + '</li>';
    });
    document.getElementById("liste").innerHTML = '</ul>';
}

function createListFromJson(){
    var id = "liste";
    var tabl = loadJson();
    createList(tabl, id);
    
}
