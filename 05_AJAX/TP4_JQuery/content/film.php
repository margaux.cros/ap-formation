<?php


$film_AA=array(
    array(
        'nomFilm' => 'Bon Dieu',
        'image' => 'bon-dieu.jpg'
    ),
    array(
        'nomFilm' => 'The grand Busapest Hotel <3',
        'image' => 'budapest-hotel.jpg'
    ),
    array(
        'nomFilm' => 'Captain America',
        'image' => 'captain-america-2.jpg'
    ),
    array(
        'nomFilm' => 'Grace',
        'image' => 'grace.jpg'
    ),
    array(
        'nomFilm' => 'Rio 2',
        'image' => 'rio-2.jpg'
    ),
    array(
        'nomFilm' => 'X-Men',
        'image' => 'xmen.jpg'
    ),
    array(
        'nomFilm' => 'Yeux Jaunes',
        'image' => 'yeux-jaunes.jpg'
    )

);

echo '<div class="row">';

foreach($film_AA as $film_A){
    echo'
    <div class="box-film">
       <div class="img-film">
            <img src="images/' . $film_A['image'] . '"/>
       </div>
       <div class="film-title">
            <h2>'. $film_A['nomFilm'] . '</h2>
       </div>
    </div>
    ';
}

echo '</div>';
