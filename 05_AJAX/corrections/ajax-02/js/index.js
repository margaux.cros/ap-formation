document.getElementById("output-content-no").addEventListener("click", sendRequest);
document.getElementById("output-content").addEventListener("click", sendRequestApi);

let target = document.getElementById("receive");
let image  = document.getElementById("image");


async function sendRequestApi() {
    //Get data FROM API
    fetch('https://yesno.wtf/api?ref=apilist.fun')

        //Put data in JSON
        .then(response => response.json())

        //Output data
        .then((response) => {
            image.src = response.image;
        })
        .catch(function (err) {
            console.log(err)
        })
}


async function sendRequest() {

    //Get file content
    fetch('../content.json', {
        method: "GET",
        headers: {
            "Content-Type": "application/text"
        }
    })

        //Put data in JSON
        .then(response   => response.json())

        //Output data
        .then((response) => {

            target.innerHTML =
                "<li class='list-group-item text-center mt-2 text-light bg-dark'>" + response.data.text_1 + "</li>" +
                "<li class='list-group-item text-center mt-2 text-light bg-primary'>" + response.data.text_2 + "</li>" +
                "<li class='list-group-item text-center mt-2 text-light bg-dark'>" + response.data.text_3 + "</li>"
            ;

        })

        //Catch error
        .catch(function (err) {
            console.log(err)
        })

}
