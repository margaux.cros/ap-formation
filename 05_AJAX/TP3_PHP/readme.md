# Ajax
 interface PHP
## Enoncé
en php on déclare un tableau de prénoms.
Le but est que dans une zone de texte html dès lors que l'on tape une lettre la suggestion de prénom apparaisse
S'il n'y a pas de solutions, le texte "no suggestion" doit être affiché à la place
​
## Consignes
- le php doit fournir ce service (récupérer une liste de prénoms commençant par une lettre ou plus)
ex: liste initiale:
["Anna", "Brittany", "Cinderella", "Diana", "Eva", "Fiona", "Gunda", "Hege", "Inga",
	"Johanna", "Kitty", "Linda", "Nina", "Ophelia", "Petunia", "Amanda", "Raquel", "Cindy",
	"Doris", "Eve", "Evita", "Sunniva", "Tove", "Unni", "Violet", "Liza",
	"Elizabeth", "Ellen", "Wenche", "Vicky"]
	
E ramène Eva, Eve, Evita, Elizabeth, Ellen
...
## Détails