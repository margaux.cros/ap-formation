### Classes, Héritage
Gérer les ventes et coûts de fabrication de produits d'une boulangerie "La Grande Boulangerie"
### data
4 employés:
- Violaine, Virginie : vendeuse
- Bernard: boulanger
- Paul : Patissier
Un patissier est un boulanger. Vendeuse et boulanger sont des employés
On produit (cout de fabrication, prix de vente):
- baguette (0.10, 0.78) 
- croissant (0.15, 1.10)
- pain de mie (0.35, 2.50)
- charlotte (6.20, 30.00) (la charlotte est un produit spécial (une patisserie))
Bernard fabrique 80 baguettes&20 pains de mie
Paul 60 croissants&5 charlottes
Violaine vend 75 baguettes/50 croissants 
Virginie 20 pains de mie+4charlottes

### tracer dans la console les faits marquants (construction, héritage, ventes, fabrications, ...) de cette gestion