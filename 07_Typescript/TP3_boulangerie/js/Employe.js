var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * abstract class Employe
 */
var Employe = /** @class */ (function () {
    function Employe(prenom) {
        this.prenom = prenom;
    }
    Employe.prototype.getPrenom = function () {
        return this.prenom;
    };
    Employe.prototype.strPrenom = function () {
        return 'bonjour je m\'appelle ' + this.getPrenom();
    };
    return Employe;
}());
/**
 * class Vendeuse
 */
var Vendeuse = /** @class */ (function (_super) {
    __extends(Vendeuse, _super);
    function Vendeuse(prenom) {
        return _super.call(this, prenom) || this;
    }
    return Vendeuse;
}(Employe));
/**
 * class Boulanger
 */
var Boulanger = /** @class */ (function (_super) {
    __extends(Boulanger, _super);
    function Boulanger(prenom) {
        return _super.call(this, prenom) || this;
    }
    return Boulanger;
}(Employe));
/**
 * class Patissier
 */
var Patissier = /** @class */ (function (_super) {
    __extends(Patissier, _super);
    function Patissier(prenom) {
        return _super.call(this, prenom) || this;
    }
    return Patissier;
}(Boulanger));
