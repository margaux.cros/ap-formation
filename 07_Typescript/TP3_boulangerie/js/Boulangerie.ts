class Boulangerie {
    private enseigne;
    private equipe;
    private stock;

    constructor(enseigne){
        this.enseigne = enseigne;
        this.equipe = [];
        this.stock = [];
    }

    getEnseigne(){
        return this.enseigne;
    }

    ajouterEmploye(poste, prenom){

        switch(poste.toLowerCase()) {
            case 'vendeuse' :
            var employe = new Vendeuse(prenom);
            return employe.strPrenom();
            case 'boulanger':
            var employe = new Boulanger(prenom);
            return employe.strPrenom();
            case 'patissier' :
            var employe = new Patissier(prenom);
            return employe.strPrenom();

            this.equipe.push(employe)
        }

    }

    strEnseigne(){
        return 'Bienvenue chez '+this.getEnseigne() +'!';
    }

    produire(employe, nombre, type){
        employe.fabriquer(nombre, type);
    }

    vendre(employe, nombre, type){
        employe.vendre(nombre, type);
    }
}
