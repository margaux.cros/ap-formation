/**
 * abstract class Employe
 */
abstract class Employe {

    private prenom;
    private stock;

    constructor(prenom){
        this.prenom = prenom;
    }

    getPrenom(){
        return this.prenom;
    }

    strPrenom(){
        return 'Bonjour, je m\'appelle ' + this.getPrenom();
    }
}

/**
 * class Vendeuse
 */
class Vendeuse extends Employe{
    constructor(prenom){
        super(prenom);
    }

    vendre
}

/**
 * class Boulanger
 */
class Boulanger extends Employe{
    constructor(prenom){
        super(prenom);
    }

    fabriquer(nombre, type){
        var fournee  = new Fournee(nombre, type);
        console.log(fournee.strFournee());
    }

}

/**
 * class Patissier
 */
class Patissier extends Boulanger{
    constructor(prenom){
        super(prenom);
    }

}

