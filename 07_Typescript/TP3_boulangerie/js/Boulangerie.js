var Boulangerie = /** @class */ (function () {
    function Boulangerie(enseigne) {
        this.enseigne = enseigne;
    }
    Boulangerie.prototype.getEnseigne = function () {
        return this.enseigne;
    };
    Boulangerie.prototype.ajouterEmploye = function (poste, prenom) {
        switch (poste.toLowerCase()) {
            case 'vendeuse':
                var employe = new Vendeuse(prenom);
                return employe.strPrenom();
            case 'boulanger':
                var employe = new Boulanger(prenom);
                return employe.strPrenom();
            case 'patissier':
                var employe = new Patissier(prenom);
                return employe.strPrenom();
                this.equipe.push(employe);
        }
    };
    Boulangerie.prototype.strEnseigne = function () {
        return 'Bienvenue chez ' + this.getEnseigne() + '!';
    };
    return Boulangerie;
}());
