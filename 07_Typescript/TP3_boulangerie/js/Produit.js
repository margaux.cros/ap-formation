var Produit = /** @class */ (function () {
    // - baguette (0.10, 0.78)
    // - croissant (0.15, 1.10)
    // - pain de mie (0.35, 2.50)
    // - charlotte (6.20, 30.00) (la charlotte est un produit spécial (une patisserie))
    function Produit(type) {
        this.type = type;
        switch (this.type) {
            case 'baguette':
                this.coutFabrication = 0.10;
                this.prixVente = 0.78;
                break;
            case 'baguette':
                this.coutFabrication = 0.15;
                this.prixVente = 1.10;
                break;
            case 'pain de mie':
                this.coutFabrication = 0.35;
                this.prixVente = 2.50;
                break;
            case 'charlotte':
                this.coutFabrication = 6.20;
                this.prixVente = 30.00;
                break;
        }
    }
    Produit.prototype.getType = function () {
        return this.type;
    };
    Produit.prototype.getcoutFabrication = function () {
        return this.coutFabrication;
    };
    Produit.prototype.getprixVente = function () {
        return this.prixVente;
    };
    return Produit;
}());
var ProduitFactory = /** @class */ (function () {
    function ProduitFactory(nombre, type) {
        var i = 0;
        while (i < nombre) {
            var produit = new Produit(type);
            this.fournee.push(produit);
            i++;
        }
    }
    ProduitFactory.prototype.getFournee = function () {
        return this.fournee;
    };
    return ProduitFactory;
}());
