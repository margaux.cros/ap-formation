abstract class Geometric {
    protected color;

    abstract perim();

    getColor(){
        return this.color;
    }

    constructor(color){
        this.color = color;
    }
}

class Rectangle extends Geometric{
    protected l;
    protected L;

    constructor (color: string, L: number, l: number){
        super(color);
        this.L = L;
        this.l = l;
    }

    perim(){
        return ((this.l + this.L) * 2);
    }
}

class Circle extends Geometric{
    protected r ;

    constructor(color: string, r: number){
        super(color);
        this.r = r ;
    }

    perim(){
        return (2 * Math.PI * this.r);
    }
}


class Square extends Rectangle{

    constructor(color: string, c: number){
        super(color, c, c);
    }
}
