var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Geometric = /** @class */ (function () {
    function Geometric(color) {
        this.color = color;
    }
    Geometric.prototype.getColor = function () {
        return this.color;
    };
    return Geometric;
}());
var Rectangle = /** @class */ (function (_super) {
    __extends(Rectangle, _super);
    function Rectangle(color, L, l) {
        var _this = _super.call(this, color) || this;
        _this.L = L;
        _this.l = l;
        return _this;
    }
    Rectangle.prototype.perim = function () {
        return ((this.l + this.L) * 2);
    };
    return Rectangle;
}(Geometric));
var Circle = /** @class */ (function (_super) {
    __extends(Circle, _super);
    function Circle(color, r) {
        var _this = _super.call(this, color) || this;
        _this.r = r;
        return _this;
    }
    Circle.prototype.perim = function () {
        return (2 * Math.PI * this.r);
    };
    return Circle;
}(Geometric));
var Square = /** @class */ (function (_super) {
    __extends(Square, _super);
    function Square(color, c) {
        return _super.call(this, color, c, c) || this;
    }
    return Square;
}(Rectangle));
