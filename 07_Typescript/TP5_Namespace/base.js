"use strict";
exports.__esModule = true;
var Animal = /** @class */ (function () {
    function Animal() {
        console.log("I am an animal");
    }
    Animal.prototype.move = function () { };
    Animal.prototype.shout = function () { console.log("I shout"); };
    return Animal;
}());
exports.Animal = Animal;
var Plant = /** @class */ (function () {
    function Plant() {
    }
    Plant.prototype.photosynthesize = function () { };
    return Plant;
}());
exports.Plant = Plant;
