function change(cell) {
    var rectangle = document.getElementById('rectangle');

    if (rectangle.style.display == 'none') {
        rectangle.style.display = 'block';
        cell.innerHTML = 'masquer';
    } else {
        rectangle.style.display = 'none';
        cell.innerHTML = 'afficher';
    }
}

function rectOver(rectangle) {
    rectangle.classList.add('important');
    rectangle.classList.remove('royalblue');
    rectangle.classList.remove('good');
}

function rectOut(rectangle) {
    rectangle.classList.add('royalblue');
    rectangle.classList.remove('important');
    rectangle.classList.remove('good');
}

function rectDb(rectangle){
    if (rectangle.classList.contains('royalblue')) {
        rectangle.classList.add('good');
        rectangle.classList.remove('royalblue');
    } else if (rectangle.classList.contains('important')) {
        rectangle.classList.add('good');       
        rectangle.classList.remove('important');      
    } else {
        rectangle.classList.add('important');
        rectangle.classList.remove('good');
    }
}
