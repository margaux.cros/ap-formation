function main() {
    var choice = prompt('Pierre, Feuille, Ciseau !?');

    var c = choice.toLocaleLowerCase();
    //foutre tout en minuscules
    shifumi(c);




} //end function main()


function shifumi(g) {

    if (!verifGeste(g)) {
        document.write('<p>veuillez saisir un geste valide </p>');
    } else {
        document.write('<p> le joueur a choisi : ' + g + '</p>');

        //ordinateur
        var gestes_A = ['pierre', 'feuille', 'ciseau'];
        var tirage = Math.floor(Math.random() * 3);
        var ia = gestes_A[tirage];
        document.write('<p> l\'ordinateur a choisi : ' + ia + '</p>');

        //resultat
        if (g == 'pierre') {
            if (ia == 'pierre') {
                document.write('<p> égalité </p>');
            } else if (ia == 'feuille') {
                document.write('<p> perdu </p>');
            } else if (ia == 'ciseau') {
                document.write('<p> gagné </p>');
            }
        } else if (g == 'feuille') {
            if (ia == 'pierre') {
                document.write('<p> gagné </p>');
            } else if (ia == 'feuille') {
                document.write('<p> égalité </p>');
            } else if (ia == 'ciseau') {
                document.write('<p> perdu </p>');
            }
        } else if (g == 'ciseau') {
            if (ia == 'pierre') {
                document.write('<p> perdu</p>');
            } else if (ia == 'feuille') {
                document.write('<p> perdu </p>');
            } else if (ia == 'ciseau') {
                document.write('<p> égalité </p>');
            }
        }
    }
}

function verifGeste(g) {

    if (g == 'pierre' || g == 'ciseau' || g == 'feuille') {
        return true;
    }
    return false;
}
