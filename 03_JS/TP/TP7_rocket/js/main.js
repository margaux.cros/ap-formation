'use strict';


var spanCompt = document.getElementById("compteur");

var stopRocket_B = false;

var restartFire_B = false;



function fire() {

    var compteur = 10;
    spanCompt.innerHTML = compteur;
    var id = setInterval(timerRocket, 1000);

    function timerRocket() {
        if (stopRocket_B == true) {
            clearInterval(id);
            document.getElementById("rocket").src = 'images/rocket1.png';
            stopRocket_B = false;
        } else if (restartFire_B == true) {
            compteur = 10;
            spanCompt.innerHTML = compteur;
            restartFire_B =false;
            fire();
        } else if (compteur == 0) {
            clearInterval(id);
            goAway()
        } else {
            compteur--;
            spanCompt.innerHTML = compteur;
        }
    }
}

function init() {
    document.getElementById("rocket").src = 'images/rocket2.gif';
    fire();
}

function goAway() {
    var rocket = document.getElementById("rocket");
    rocket.src = 'images/rocket3.gif';
    rocket.classList.add('tookOff');
}

function stopRocket() {
    stopRocket_B = true;
}

function restartFire() {
    restartFire_B = true;
}




/************************************************************************************/
/* ******************************** CODE PRINCIPAL **********************************/
/************************************************************************************/
