function main() {

    var a = prompt('nombre 1');
    var o = prompt('opération (+, -, *, /, ^)')
    var b = prompt('nombre 2');

    if (verifFloat(a) || verifFloat(b)) {
        a = parseFloat(a);
        b = parseFloat(b);

        var r = calculatrice(a, b, o);
        document.write('<p>' + r + '</p>');

    } else {
        document.write('<p> veuillez saisir des nombres </p>');
    }


} //end function main()

function calculatrice(a, b, o) {

    if (o == '+' || o == 'addition') {
        return (a + b);
    } else if (o == '-' || o == 'soustraction') {
        return (a - b);
    } else if (o == '*' || o == 'multiplication') {
        return (a * b);
    } else if (o == '/' || o == 'division') {
        if (b == 0) {
            return 'erreur : division par 0 impossible';
        }
    } else if (o == '^' || o == 'puissance') {
        if (a > 0 && (b < 1 && b > 0)) {
            return Math.pow(a, b);
        } else {
            return 'erreur : racine carrée d\'un nombre positif impossible';
        }
    } else {
        return 'erreur : opération mathématique inconnue';
    }
}

function verifFloat(n) {
    if (parseFloat(n) == n) {
        return true;
    }
    return false;
}
