var slider = document.getElementById("slider");
var buttonToolbar = document.getElementById("butt-toolbar");
var toolbar = document.getElementById("box-toolbar");
var icon = document.getElementById("toolbar-icon");
var toogleToolbar_B = 0;
var slide = document.getElementById("slide");


document.getElementById("butt-toolbar").addEventListener("click", function () {
    if (!toogleToolbar_B) {
        toolbar.style.display = '';
        toogleToolbar_B = 1;
        icon.classList.remove("fa-arrow-down");
        icon.classList.add("fa-arrow-up");
    } else {
        toolbar.style.display = 'none';
        toogleToolbar_B = 0;
        icon.classList.remove("fa-arrow-up");
        icon.classList.add("fa-arrow-down");
    }
});

function getNumSlide() {
    //alert(numSlide=slide.src);
    numSlide = slide.src.substr(65, 1);
    return (numSlide);
}

function setNumSlide(num) {
    newSlide = 'http://localhost/Cours_AP_Formation/03_JS/TP/TP8_carousel/images/' + num + '.jpg';
    slide.src = newSlide;
    return true;
}

function getRandomInt(max) {
    return (Math.floor(Math.random() * Math.floor(max)) + 1);
}

function onClkSlidPrev() {
    numSlide = getNumSlide();
    if (numSlide == 1) {
        setNumSlide(6);
    } else {
        setNumSlide(parseInt(numSlide) - 1);
    }

}

function onClkSlidNext() {
    numSlide = getNumSlide();
    if (numSlide == 6) {
        setNumSlide(1);
    } else {
        setNumSlide(parseInt(numSlide) + 1);
    }
}

document.getElementById("slider-previous").addEventListener("click",  onClkSlidPrev);

document.addEventListener("keydown", function (e) {
    if (e.keyCode === 37) {
        onClkSlidPrev();
    } else if (e.keyCode === 39) {
        onClkSlidNext();
    }
});


document.getElementById("slider-next").addEventListener("click", onClkSlidNext);

document.getElementById("slider-toogle").addEventListener("click", function () {
    setInterval(function () {
        numSlide = getNumSlide();
        if (numSlide == 1) {
            setNumSlide(6);
        } else {
            setNumSlide(parseInt(numSlide) - 1);
        }
    }, 5000);
});

document.getElementById("slider-random").addEventListener("click", function () {
    var slide = getRandomInt(5);
    setNumSlide(parseInt(slide));
});
