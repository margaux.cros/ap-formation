// --------------------- CLASS ---------------------------

/**
 * class Character
 */
class Character {
    constructor(life) {
        this.life = life;
    }
}



/**
 * class Game
 */
class Game {

    constructor(level, armor, sword) {

        var defLevel_A = ['easy', 'medium', 'hard'];
        var defArmor_A = ['copper', 'iron', 'magic'];
        var defSword_A = ['wood', 'steel', 'excalibur'];

        if (defLevel_A.indexOf(level) >= 1) {
            this.level = level;
        } else {
            return 'veuillez saisir un niveau de difficulté'
        }

        if (defArmor_A.indexOf(armor) >= 1) {
            this.armor = armor;
        } else {
            return 'veuillez saisir une armure'
        }

        if (defSword_A.indexOf(sword) >= 1) {
            this.sword = sword;
        } else {
            return 'veuillez saisir une épée'
        }

        this.gameRound = 1; //init round

    }

    initWarrior() {
        var WarriorLife = 0;

        if (this.level == 'easy' || this.level == 'medium') {
            WarriorLife = 200 + (Math.floor(Math.random() * 50));
        } else {
            WarriorLife = 150 + (Math.floor(Math.random() * 50));
        }

        this.warrior = new Character(WarriorLife);

        return this.warrior;
    }

    initDragon() {
        var DragonLife = 0;

        if (this.level == 'easy') {
            DragonLife = 150 + (Math.floor(Math.random() * 50));
        } else {
            DragonLife = 150 + (Math.floor(Math.random() * 50));
        }

        this.dragon = new Character(DragonLife);

        return this.dragon;
    }


    strInitGame() {
        return (
            '<h5> DEBUT DE PARTIE</h5> <br/>' +
            '<p> ***Javawan the Bug*** :' + this.dragon.life + ' PV <br/>' +
            '<p> ***Luk Skriptwalker*** :' + this.warrior.life + ' PV <br/>'
        );
    }

    gameRound() {
        var reto = '';

        reto += '<h5> tour num' + this.gameRound + '</h5>'

        if (roundOf(this.gameRound) == 'dragon') {
            reto += 'Le dragon ***Javawan the Bug*** est plus rapide et vous inflige 14 points de dommage';
            reto += '***Javawan the Bug*** : 152 PV';
            reto += '***Luk Skriptwalker*** : 134 PV';
        } else {
            reto += 'Vous êtes plus rapide et vous infligez 24 points de dommage au dragon.';
            reto += '***Javawan the Bug*** : 75 PV ';
            reto += '***Luk Skriptwalker*** : 38PV';
        }

        return reto;
    }

    initGame() {
        o = Math.floor(Math.random() * 2)
        this.initRound = 'warrior';
        if (o > 1) {
            this.initRound = 'dragon';
        }

        return this.initRound;



    }

    roundof(n) {
        if (this.initRound == 'warrior') {
            if (n % 2 == 1) {
                return 'warrior';
            } else {
                return 'dragon';
            }
        } else {
            if (n % 2 == 1) {
                return 'dragon';
            } else {
                return 'warrior';
            }
        }
    }

    playGame() {
        while (this.warrior.life > 0 || this.dragon.life > 0) {
            gameRound();
        }

        if (this.warrior.life <= 0) {
            return 'le dragon a gagné';
        } else {
            return 'le chevalier a gagné';
        }
    }

    warriorRound() {
        var dmg = 10;

        if (this.level == 'easy') {
            if (this.sword == 'wood') {
                dmg = 15;
            } else if (this.sword == 'steel') {
                dmg = 20;
            } else {
                dmg = 25;
            }
        } else if (this.level == 'medium')  {
            if (this.sword == 'wood') {
                dmg = 10;
            } else if (this.sword == 'steel') {
                dmg = 15;

            } else {
                dmg = 20;

            }
        }else {
            if (this.sword == 'wood') {
                dmg = 5;
            } else if (this.sword == 'steel') {                
                dmg = 10;
            } else {
                dmg = 20;
            }
        }

        this.dragon.life -= dmg;

        return this.dragon.life;
    }
    dragonRound() {
        var dmg = 10;
        if (this.armor == 'copper') {

        } else if (this.armor == 'iron') {

        } else {

        }

        this.warrior.life -= dmg;

        return this.warrior.life;

    }



}




// ---------------------- MAIN JS -------------------------
function main() {

    var game = new Game('easy', 'iron', 'wood')

    var W = game.initWarrior();
    var D = game.initDragon();

    var start = game.strInitGame();
    document.write(start);




} //end function main()
