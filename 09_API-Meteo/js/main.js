document.getElementById("btn-getSelect").addEventListener("click", function(){
    fetch('http://formation.webboy.fr/liste_des_villes.php').then(function(result){
    console.log('données recupérées');
    result.json()
        .then(function(data) {
            console.log('json ok');
            console.log(data);
            document.getElementById("select").innerHTML = '<option value="0"></option>';
            data.forEach(function(value){
                document.getElementById("select").innerHTML +=  `<option value="${value.id}">${value.name}</option>`;
            });

        });
    }).catch(function (error){
        console.log('erreur');
        console.log(error);
    });
});



document.getElementById("select").addEventListener("change", function(){
    var e = document.getElementById("select");
    var cityId = e.options[e.selectedIndex].value;
    console.log(cityId);
    if(cityId > 0){
        document.getElementsByClassName('result')[0].style.display = 'block';
        fetch('http://api.openweathermap.org/data/2.5/weather?id='+cityId+'&APPID=b7e66aebecb956d08cc56313ca276b13').then(function(result){
        console.log('données recupérées');
        result.json()
            .then(function(data) {
                console.log('json ok');
                console.log(data);

                document.getElementById('data-name').innerHTML = data.name;
                document.getElementById('data-temperature').innerHTML = (data.main.temp - 273.15).toFixed(2) +' °C';
                document.getElementById('data-windspeed').innerHTML = (data.wind.speed * 3.6).toFixed(2) + ' km/h';
                document.getElementById('data-winddirection').innerHTML = '<img style="transform: rotate('+data.wind.deg+'deg)" class="max-w-30 arrow" src="images/arrow-icon.jpg">';
                document.getElementById('data-gps').innerHTML = 'GPS : '+data.coord.lon+'°N, '+data.coord.lat+'°O';
                document.getElementById('data-date').innerHTML = 'Dernier relevé : '+dateFr(data.dt);
            });
        }).catch(function (error){
            console.log('erreur');
            console.log(error);
        });
    }else{
        document.getElementsByClassName('result')[0].style.display = 'none';
    }
});


function dateFr(timestamp)
{
    date=new Date(timestamp *1000);

    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric", hour:"numeric", minute: "numeric"};
    return date.toLocaleDateString("fr-FR", options);
}





// api.openweathermap.org/data/2.5/weather?id=2172797
